package mateuszmazurek.pja.shoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import mateuszmazurek.pja.core.SharedPreferencesService;
import mateuszmazurek.pja.core.ShoppingListAdapter;
import mateuszmazurek.pja.dataaccess.ShoppingListRepository;
import mateuszmazurek.pja.model.ShoppingList;

import java.util.List;


public class MainActivity extends Activity {
    private ListView shoppingListView;
    private ImageButton addNewListBtn;
    private EditText editTextList;
    private List<ShoppingList> shoppingLists;
    private ShoppingListRepository repository;
    private ShoppingListAdapter shoppingListAdapter;
    private SharedPreferencesService preferencesService;

    private View.OnClickListener onItemDeleteClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final int position = (Integer) v.getTag();
            final ShoppingList sh = shoppingLists.get(position);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setMessage("Are you want delete: " + sh.getName() + "?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                repository.deleteShoppingList(sh);
                                shoppingLists.remove(position);
                                shoppingListAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), "We can't delete " + sh.getName(), Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            builder.create().show();
        }
    };


    private View.OnLongClickListener onItemLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            final int position = (Integer) v.getTag();
            final ShoppingList sh = shoppingLists.get(position);
            String header = "Edit " + sh.getName() + " product.";
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = MainActivity.this.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.edit_shopping_list_name,null);
            TextView msgTextView = (TextView) rowView.findViewById(R.id.editNameTextView);
            msgTextView.setText(header);
            msgTextView.setTextSize(getApplicationContext().getResources().getDimension(preferencesService.getFont()));
            msgTextView.setBackgroundColor(getApplicationContext().getResources().getColor(preferencesService.getColor()));

            final EditText nameEditText = (EditText) rowView.findViewById(R.id.editNameEditText);

            builder.setView(rowView)
                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String newName = nameEditText.getText().toString();

                            if (!newName.isEmpty()) {
                                try {
                                    sh.setName(newName);
                                    repository.editShoppingList(sh);
                                    shoppingLists.get(position).setName(newName);
                                    shoppingListAdapter.notifyDataSetChanged();
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            builder.create().show();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferencesService = new SharedPreferencesService(getApplicationContext());
        shoppingListView = (ListView) findViewById(R.id.shoppingListView);
        addNewListBtn = (ImageButton) findViewById(R.id.addNewListBtn);

        addNewListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextList == null) {
                    editTextList = ((EditText) findViewById(R.id.newShoppinglistEditText));
                }

                String nameList = editTextList.getText().toString();
                ShoppingList sh = new ShoppingList();
                sh.setName(nameList);
                try {
                    sh = repository.addShoppingList(sh);

                    shoppingLists.add(sh);
                    shoppingListAdapter.notifyDataSetChanged();
                    editTextList.setText("");
                    InputMethodManager imm = (InputMethodManager)getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editTextList.getWindowToken(), 0);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        initShoppingLists();
        initShoppingListView();
    }

    private void initShoppingListView() {
        this.shoppingListAdapter = new ShoppingListAdapter(this, shoppingLists);
        shoppingListAdapter.setOnItemDeleteClickListener(onItemDeleteClickListener);
        shoppingListAdapter.setOnItemEditLongClickListener(onItemLongClickListener);
        shoppingListView.setAdapter(shoppingListAdapter);
    }

    private void initShoppingLists() {
        repository = new ShoppingListRepository(getApplicationContext());
        repository.open();
        this.shoppingLists = repository.getAllShoppingLists();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.action_exit:
                finish();
                break;
            default:
                handled = super.onOptionsItemSelected(item);

        }

        return handled;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
