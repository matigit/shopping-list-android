package mateuszmazurek.pja.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import mateuszmazurek.pja.core.SharedPreferencesService;


public class SettingActivity extends Activity {

    private ArrayAdapter<CharSequence> colourAdapter;
    private ArrayAdapter<CharSequence> fontSizeAdapter;
    private SharedPreferencesService spService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        this.spService = new SharedPreferencesService(getApplicationContext());

        initialiseControls();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings_back:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            default:
                handled = super.onOptionsItemSelected(item);

        }

        return handled;
    }


    private void initialiseControls() {
        initialiseColourSpinner();
        initialiseFontSizeSpinner();
    }

    private void initialiseFontSizeSpinner() {
        this.fontSizeAdapter = ArrayAdapter.createFromResource(this, R.array.fontsize_droplist, android.R.layout.simple_spinner_item);

        initialiseSpinner(R.id.fontSizeSpinner, fontSizeAdapter, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    switch (position) {
                        case 1:
                            spService.setFont(R.dimen.font_small);
                            break;
                        case 2:
                            spService.setFont(R.dimen.font_normal);
                            break;
                        case 3:
                            spService.setFont(R.dimen.font_large);
                            break;
                    }
                if (position != 0) {
                    Toast.makeText(getApplicationContext(), String.format("You choose %s font size.", fontSizeAdapter.getItem(position)), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initialiseColourSpinner() {
        this.colourAdapter = ArrayAdapter.createFromResource(this, R.array.colour_droplist, android.R.layout.simple_spinner_item);

        initialiseSpinner(R.id.colourSpinner, colourAdapter, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    switch (position) {

                        case 1:
                            spService.setColor(R.color.color_blue);
                            break;
                        case 2:
                            spService.setColor(R.color.color_red);
                            break;
                }

                if (position != 0) {
                    Toast.makeText(getApplicationContext(), String.format("You choose %s color.", colourAdapter.getItem(position)), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initialiseSpinner(int idSpinner, ArrayAdapter<CharSequence> adapter, AdapterView.OnItemSelectedListener listener) {
        Spinner spinner = (Spinner) findViewById(idSpinner);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(listener);
    }

}
