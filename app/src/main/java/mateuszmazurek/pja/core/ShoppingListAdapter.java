package mateuszmazurek.pja.core;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import mateuszmazurek.pja.model.ShoppingList;
import mateuszmazurek.pja.shoppinglist.R;

import java.util.List;

/**
 * Created by Mateusz on 2014-11-02.
 */
public class ShoppingListAdapter extends ArrayAdapter<ShoppingList> {

    private List<ShoppingList> shoppingLists;
    private SharedPreferencesService preferenceService;
    private Activity context;

    private View.OnClickListener onItemDeleteClickListener;
    private View.OnLongClickListener onItemEditLongClickListener;

    public ShoppingListAdapter(Activity context, List<ShoppingList> objects) {
        super(context, R.layout.shopping_list_item, objects);

        this.preferenceService = new SharedPreferencesService(context);
        this.shoppingLists = objects;
        this.context = context;
    }

    private class ViewHolder{
        private TextView textView;
        private ImageButton imageButton;
        public ViewHolder(TextView text, ImageButton btn){
            this.textView = text;
            this.imageButton = btn;
        }

        public TextView getTextView() {
            return this.textView;
        }

        public ImageButton getImageButton() {
            return this.imageButton;
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            rowView = layoutInflater.inflate(R.layout.shopping_list_item, null, true);
            viewHolder = new ViewHolder((TextView) rowView.findViewById(R.id.shoppinListNameView), (ImageButton)rowView.findViewById(R.id.deleteShoppingListBtn));

            viewHolder.getImageButton().setOnClickListener(new
                View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemDeleteClickListener != null) {
                            onItemDeleteClickListener.onClick(v);
                        }
                    }
                });
            viewHolder.getTextView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onItemEditLongClickListener != null) {
                        onItemEditLongClickListener.onLongClick(v);
                        return true;
                    }

                    return false;
                }
            });

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.getTextView().setText(shoppingLists.get(position).getName());
        viewHolder.getTextView().setBackgroundColor(context.getResources().getColor(preferenceService.getColor()));
        viewHolder.getImageButton().setBackgroundColor(context.getResources().getColor(preferenceService.getColor()));
        viewHolder.getTextView().setTextSize(context.getResources().getDimension(preferenceService.getFont()));
        viewHolder.getImageButton().setTag(new Integer(position));
        viewHolder.getTextView().setTag(new Integer(position));

        return rowView;
    }

    public void setOnItemDeleteClickListener(View.OnClickListener onItemDeleteClickListner) {
        this.onItemDeleteClickListener = onItemDeleteClickListner;
    }

    public void setOnItemEditLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onItemEditLongClickListener = onLongClickListener;
    }
}
