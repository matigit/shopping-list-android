package mateuszmazurek.pja.core;

import android.content.Context;
import android.content.SharedPreferences;
import mateuszmazurek.pja.shoppinglist.R;

/**
 * Created by Mateusz on 2014-11-08.
 */
public class SharedPreferencesService {
    private final String COLOUR_KEY = "colour_key";
    private final String FONT_KEY = "font_key";


    private SharedPreferences sharedPreferences;

    public SharedPreferencesService(Context context) {
        this.sharedPreferences = context.getSharedPreferences(context.getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
    }


    public void setColor(int idColor) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COLOUR_KEY, idColor);

        editor.commit();
    }

    public int getColor() {
        return sharedPreferences.getInt(COLOUR_KEY, R.color.color_blue);
    }

    public void setFont(int idFont) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(FONT_KEY, idFont);
        editor.commit();
    }

    public int getFont() {
        return sharedPreferences.getInt(FONT_KEY, R.dimen.font_normal);
    }


}
