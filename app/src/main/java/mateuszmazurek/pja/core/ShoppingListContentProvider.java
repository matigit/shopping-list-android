package mateuszmazurek.pja.core;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import mateuszmazurek.pja.dataaccess.ShoppingDatabaseHelper;
import mateuszmazurek.pja.dataaccess.ShoppingListRepository;
import mateuszmazurek.pja.model.Product;
import mateuszmazurek.pja.model.ShoppingList;

import java.util.HashSet;

/**
 * Created by Mateusz on 2014-10-26.
 */
public class ShoppingListContentProvider extends ContentProvider {

    // database
    private ShoppingListRepository database;

    // used for the UriMacher
    private final int SHOPPINGLISTS = 1;
    private final int SHOPPINGLIST_ID = 2;

    private final String AUTHORITY = "mateuszmazurek.pja.shoppinglist.provider";

    private final String SHOPPINGLIST_PATH = "shoppinglists";

    private final UriMatcher sURIMatcher;


    public ShoppingListContentProvider() {
        this.sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        setUriMatcher();
    }

    private void setUriMatcher() {
        sURIMatcher.addURI(AUTHORITY, SHOPPINGLIST_PATH, SHOPPINGLISTS);
        sURIMatcher.addURI(AUTHORITY, SHOPPINGLIST_PATH + "/#", SHOPPINGLIST_ID);
    }



    @Override
    public boolean onCreate() {
        try {
            database = new ShoppingListRepository(getContext());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        database.open();
        return database.getAllShoppingList();
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        database.open();
        long id = 0;
        Uri returnURi = null;

        switch (uriType) {
            case SHOPPINGLISTS:
                if (values.containsKey("name")) {
                    ShoppingList shoppingList = new ShoppingList();
                    shoppingList.setName(values.getAsString("name"));
                    try {
                        id = database.addShoppingList(shoppingList).getId();
                        returnURi = Uri.parse(SHOPPINGLIST_PATH + "/" + id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnURi;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

       return 0;
    }

}
