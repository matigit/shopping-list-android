package mateuszmazurek.pja.model;

/**
 * Created by Mateusz on 2014-10-31.
 */
public class Product {
    private long id;
    private String name;
    private double price;

    public Product(long id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Product(){}

    public void setId(long id)
    {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getId() {
        return  id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return  price;
    }
}
