package mateuszmazurek.pja.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Mateusz on 2014-10-31.
 */
public class ShoppingList {
    private long id;
    private String name;
    private Collection<Product> products;

    public ShoppingList()
    {}

    public ShoppingList(long id,String name)
    {
        this.id = id;
        this.name = name;
        this.products = new HashSet<Product>();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return  id;
    }

    public String getName() {
        return name;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products.addAll(products);
    }
}
