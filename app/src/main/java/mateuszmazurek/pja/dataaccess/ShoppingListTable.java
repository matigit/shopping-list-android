package mateuszmazurek.pja.dataaccess;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Mateusz on 2014-10-26.
 */
public class ShoppingListTable {

    public static final String TABLE_SHOPPINGLIST = "shoppinglist";
    public static final String COLUMN_ID = "_ID";
    public static final String COLUMN_NAME = "name";
    public static final int TABLE_VERSION = 1;

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_SHOPPINGLIST
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null"
            + ");"
            + "create index shoppinglistindex on " + TABLE_SHOPPINGLIST + "(" + COLUMN_ID + ");";

    private static final String DEFAULT_LIST =
              "INSERT INTO " + TABLE_SHOPPINGLIST + " VALUES(1, \"Milk\");"
            + "INSERT INTO " + TABLE_SHOPPINGLIST + " VALUES(2, \"Coca-cola\");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
        database.execSQL(DEFAULT_LIST);
    }
}
