package mateuszmazurek.pja.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import mateuszmazurek.pja.model.Product;
import mateuszmazurek.pja.model.ShoppingList;

import java.util.*;


/**
 * Created by Mateusz on 2014-10-31.
 */
public class ShoppingListRepository{
    private SQLiteDatabase db;
    private Context context;

    public ShoppingListRepository(Context context)
    {
        this.context = context;
    }

    public void open() {
        ShoppingDatabaseHelper helper = new ShoppingDatabaseHelper(context);
        try {
            db = helper.getWritableDatabase();
        }
        catch (SQLiteException ex){
            db = helper.getReadableDatabase();
        }
    }

    public void close()
    {
        db.close();
    }


    public ShoppingList addShoppingList(ShoppingList shoppingList) throws Exception {
        if (shoppingList.getName() == null || shoppingList.getName().isEmpty()) {
            throw new Exception("You need define name of shopping list");
        }

        ContentValues cv = getContentValuesShoppingList(shoppingList);

        shoppingList.setId(db.insert(ShoppingListTable.TABLE_SHOPPINGLIST, null, cv));

        return shoppingList;
    }




    public void deleteShoppingList(ShoppingList shoppingList) throws Exception {
        if (shoppingList.getId() == 0) {
            throw new Exception("Error");
        }

        String selection = ShoppingListTable.COLUMN_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(shoppingList.getId())};

        db.delete(ShoppingListTable.TABLE_SHOPPINGLIST, selection, selectionArgs);
    }

    public ShoppingList editShoppingList(ShoppingList shoppingList) throws Exception {
        if (shoppingList.getId() == 0) {
            throw new Exception("You need save shopping list");
        }

        ContentValues cv = getContentValuesShoppingList(shoppingList);
        String selection = ShoppingListTable.COLUMN_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(shoppingList.getId())};

        db.update(ShoppingListTable.TABLE_SHOPPINGLIST, cv, selection, selectionArgs);

        return  shoppingList;
    }



    public Cursor getShoppingListById(long id) {
        String[] projection = {
                ShoppingListTable.COLUMN_ID,
                ShoppingListTable.COLUMN_NAME
        };

        String selection = ShoppingListTable.COLUMN_ID + "=?";
        String[] selectionArgs = { String.valueOf(id) };

        return db.query(ShoppingListTable.TABLE_SHOPPINGLIST, projection, selection, selectionArgs, null, null, null);
    }



    public ArrayList<ShoppingList> getAllShoppingLists() {
        ArrayList<ShoppingList> shList = new ArrayList<ShoppingList>();

        Cursor c = getAllShoppingList();

        while (c.moveToNext()) {
            ShoppingList shoppingList = new ShoppingList(c.getLong(0), c.getString(1));
            shList.add(shoppingList);
        }

        return shList;
    }

    public ShoppingList[] getAllShoppingListsArray() {
        List<ShoppingList> shList = getAllShoppingLists();

        return shList.toArray(new ShoppingList[shList.size()]);
    }

    public Cursor getAllShoppingList() {
        String[] projection = {
                ShoppingListTable.COLUMN_ID,
                ShoppingListTable.COLUMN_NAME
        };

        return  db.query(ShoppingListTable.TABLE_SHOPPINGLIST, projection, null, null, null, null, null);
    }


    private ContentValues getContentValuesShoppingList(ShoppingList shoppingList) {
        ContentValues cv = new ContentValues();
        cv.put(ShoppingListTable.COLUMN_NAME, shoppingList.getName());

        return  cv;
    }
}
